<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "usuarios".
 *
 * @property int $codigoUsuario
 * @property string $nombre
 * @property string $poblacion
 * @property string $sexo
 *
 * @property Alquileres[] $alquileres
 */
class Usuarios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'usuarios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'poblacion'], 'string', 'max' => 50],
            [['sexo'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigoUsuario' => 'Codigo Usuario',
            'nombre' => 'Nombre',
            'poblacion' => 'Poblacion',
            'sexo' => 'Sexo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlquileres()
    {
        return $this->hasMany(Alquileres::className(), ['usuario' => 'codigoUsuario']);
    }
}
